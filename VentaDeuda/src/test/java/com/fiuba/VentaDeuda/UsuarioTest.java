package com.fiuba.VentaDeuda;

import com.fiuba.VentaDeuda.domain.Usuario;
import com.fiuba.VentaDeuda.exceptions.CuitInvalidoException;
import com.fiuba.VentaDeuda.exceptions.DeudaNoDisponibleException;
import com.fiuba.VentaDeuda.exceptions.SaldoNegativoException;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UsuarioTest {

    @Test
    public void Test01UnUsuarioNoPuedeRegistrarseConUnCuitInvalido(){
        Usuario usuario = new Usuario();
        usuario.setCuit("1111111111");
        assertFalse(usuario.isValidCUITCUIL());
    }

    @Test
    public void Test02UnUsuarioNoPuedeCargarSaldoNegativo(){
        Usuario usuario = new Usuario();
        assertThrows(SaldoNegativoException.class, () -> usuario.validarMonto(new BigDecimal(-200)));
    }
}
