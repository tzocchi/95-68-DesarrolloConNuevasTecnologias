package com.fiuba.VentaDeuda;

import com.fiuba.VentaDeuda.domain.Deuda;
import com.fiuba.VentaDeuda.domain.Usuario;
import com.fiuba.VentaDeuda.enums.EstadoDeuda;
import com.fiuba.VentaDeuda.exceptions.DeudaCaducadaException;
import com.fiuba.VentaDeuda.exceptions.DeudaNoDisponibleException;
import com.fiuba.VentaDeuda.exceptions.PrecioInvalidoException;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class DeudaTest {

    @Test
    public void Test01NoSePermitenDeudasExpiradas (){
        Usuario usuario = new Usuario();
        Deuda deuda = new Deuda();
        deuda.setEstado(EstadoDeuda.NO_VENDIDO);
        deuda.setEmision(LocalDate.of(2019,05,25));
        deuda.setVendedor(usuario);
        deuda.setPublicacion(LocalDate.now());
        assertThrows(DeudaCaducadaException.class, () -> deuda.comprobarCaducidad());
    }

    @Test
    public void Test02UnaDeudaNoEstaDisponibleDespuesDeVenderse(){
        Usuario usuario1 = new Usuario();
        Deuda deuda = new Deuda();
        deuda.setEstado(EstadoDeuda.NO_VENDIDO);
        deuda.realizarVenta(usuario1);

        assertThrows(DeudaNoDisponibleException.class, () -> deuda.comprobarDisponibilidad());
    }

    @Test
    public void Test03UnaDeudaNoPuedeCostarMasQueSuGanancia(){
        Deuda deuda = new Deuda();
        deuda.setPrecio(new BigDecimal(3000));
        deuda.setValor(new BigDecimal(1000));
        assertThrows(PrecioInvalidoException.class, () -> deuda.comprobarPrecio());
    }

    @Test
    public void Test04UnaDeudaExpiraLuegoDeUnaSemanaPublicada(){
        Deuda deuda = new Deuda();
        deuda.setEstado(EstadoDeuda.NO_VENDIDO);
        deuda.setEmision(LocalDate.of(2022,05,25));
        deuda.setPublicacion(LocalDate.of(2022,07,06));
        deuda.comprobarExpiracion();
        assertEquals(EstadoDeuda.VENCIDA, deuda.getEstado());
    }

}
