package com.fiuba.VentaDeuda.exceptions;

public class PrecioInvalidoException extends RuntimeException{

    public PrecioInvalidoException (String mensaje) { super(mensaje);}
}
