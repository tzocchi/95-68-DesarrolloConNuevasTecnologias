package com.fiuba.VentaDeuda.service;

import com.fiuba.VentaDeuda.domain.Deuda;
import com.fiuba.VentaDeuda.domain.Usuario;

import java.util.List;

public interface DeudaService {

    Deuda guardar(Deuda deuda);

    Deuda encontrarDeuda(Long id);

    List<Deuda> listarDeudasDisponibles();

    void deudaComprada(Deuda deuda, Usuario usuario);

    void crearDeuda(Deuda deuda, Usuario usuario);
}
