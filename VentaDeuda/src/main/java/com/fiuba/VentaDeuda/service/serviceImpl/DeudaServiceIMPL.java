package com.fiuba.VentaDeuda.service.serviceImpl;

import com.fiuba.VentaDeuda.common.EntityDTOConverter;
import com.fiuba.VentaDeuda.dao.DeudaDAO;
import com.fiuba.VentaDeuda.domain.Deuda;
import com.fiuba.VentaDeuda.domain.Usuario;
import com.fiuba.VentaDeuda.enums.EstadoDeuda;
import com.fiuba.VentaDeuda.service.DeudaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class DeudaServiceIMPL implements DeudaService {

    @Autowired
    private DeudaDAO deudaDAO;

    @Override
    @Transactional
    public Deuda guardar(Deuda deuda) {
        return (deudaDAO.save(deuda));
    }

    @Override
    @Transactional
    public Deuda encontrarDeuda(Long id) {
        return(deudaDAO.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public List<Deuda> listarDeudasDisponibles() {
        List<Deuda> deudas = deudaDAO.findAll();
        List<Deuda> deudasDisponibles = new ArrayList<>();
        for (Deuda deuda: deudas){
            deuda.comprobarExpiracion();
            if(deuda.getEstado() == EstadoDeuda.NO_VENDIDO){
                deudasDisponibles.add(deuda);
            }
        }
        return deudasDisponibles;
    }

    @Override
    @Transactional
    public void deudaComprada(Deuda deuda, Usuario usuario) {
        deuda.comprobarDisponibilidad();
        deuda.getVendedor().realizarVenta(deuda);
        deuda.realizarVenta(usuario);
    }

    @Override
    @Transactional
    public void crearDeuda(Deuda deuda, Usuario usuario) {
        deuda.comprobarCaducidad();
        deuda.comprobarPrecio();
        deuda.crearDeuda(usuario);
        deudaDAO.save(deuda);
    }
}
