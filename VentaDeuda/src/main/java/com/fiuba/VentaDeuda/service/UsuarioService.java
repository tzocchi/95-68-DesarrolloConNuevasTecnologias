package com.fiuba.VentaDeuda.service;

import com.fiuba.VentaDeuda.domain.Deuda;
import com.fiuba.VentaDeuda.domain.Usuario;

import java.math.BigDecimal;

public interface UsuarioService {

    Usuario guardar(Usuario usuario);

    Usuario encontrarUsuario(Long idUsuario);

    Usuario findByUserName(String userName);

    Usuario crearUsuario(Usuario usuario);

    void realizarCarga(Usuario usuario, BigDecimal saldo);

    void realizarCompra(Usuario usuario, Deuda deuda);

    void publicarDeuda(Usuario usuario, Deuda deuda);
}
